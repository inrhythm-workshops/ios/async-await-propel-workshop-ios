//
//  ContentView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 2/24/23.
//

import SwiftUI

public struct ContentView: View {
    @StateObject var viewModel: SearchBarViewModel
        
    public var body: some View {
        VStack {
            DataTestView(viewModel: viewModel)
        }
        .padding()
    }
}

//
//  YFFetchErrors.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

enum YahooFinanceServiceErrors: Error {
    case YFQuoteSummaryFetching(reason: String?)
    case YFQuoteSummaryRequestMaker(reason: String?)
    case YFQuoteTopHoldingsFetching(reason: String?)
    case YFQuoteTopHoldingsRequestMaker(reason: String?)
}

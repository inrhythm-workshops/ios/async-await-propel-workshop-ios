//
//  DependencyResolver.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/28/23.
//

import Foundation

public class DependencyResolver: DependencyResolving {
    
    public func resolveYFSummarFetching() -> YFQuoteSummaryFetching {
        YFQuoteSummaryFetcher(dependencyResolver: self)
    }
    
    public func resolveYFTopHoldingsFetching() -> YFQuoteTopHoldingsFetching {
        YFQuoteTopHoldingsFetcher(dependencyResolver: self)
    }
    
    public func resolveRequestSending() -> RequestSending {
        RequestSender()
    }
}

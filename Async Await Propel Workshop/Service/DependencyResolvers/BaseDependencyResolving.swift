//
//  BaseDependencyResolving.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

public protocol BaseDependencyResolving {
    func resolveYFSummarFetching() -> YFQuoteSummaryFetching
    func resolveYFTopHoldingsFetching() -> YFQuoteTopHoldingsFetching
    func resolveRequestSending() -> RequestSending
}

//
//  DependencyResolving.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/28/23.
//

import Foundation

public protocol DependencyResolving: BaseDependencyResolving {
    func resolveYFSummarFetching() -> YFQuoteSummaryFetching
    func resolveYFTopHoldingsFetching() -> YFQuoteTopHoldingsFetching
}

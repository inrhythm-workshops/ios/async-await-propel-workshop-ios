//
//  YFQuoteSummaryFetcher.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

// Class will conform to `YFQuoteSummaryFetching` protocol.

public class YFQuoteSummaryFetcher: YFQuoteSummaryFetching {
    private var dependencyResolver: DependencyResolving
    private var requestSender: RequestSending
    
    init(dependencyResolver: DependencyResolving) {
        self.dependencyResolver = dependencyResolver
        self.requestSender = dependencyResolver.resolveRequestSending()
    }
    
    public func fetchSummary(symbolValue: String) async throws -> SummaryResponse {
        // TODO: Async/Await code goes here
        fatalError("This method is not implemented yet.")
    }
    
    func makeQuoteSummaryRequest(symbolValue: String) -> RequestMaker {
        var urlComponent = YahooFinanceURL.quoteSummary.components
        let queryItem: URLQueryItem = URLQueryItem(name: "symbol", value: symbolValue)
        urlComponent.queryItems = [queryItem]
        let requestMaker: RequestMaker = RequestMaker(urlComponent: urlComponent)
        requestMaker.assign(httpRequestMethod: .get)
        requestMaker.assign(headers: YahooResource.YFRapidAPIProd.rapidAPIProdAuth)
        return requestMaker
    }
}

//
//  YFQuoteSummaryFetching.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

public protocol YFQuoteSummaryFetching {
    func fetchSummary(symbolValue: String) async throws -> SummaryResponse
}

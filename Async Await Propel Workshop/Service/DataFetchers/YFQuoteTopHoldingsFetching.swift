//
//  YFQuoteTopHoldingsFetching.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

public protocol YFQuoteTopHoldingsFetching {
    func fetchTopHoldings(symbolValue: String) async throws -> TopHoldingsResponse
}

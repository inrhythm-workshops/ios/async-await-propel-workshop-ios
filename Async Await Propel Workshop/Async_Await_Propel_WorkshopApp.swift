//
//  Async_Await_Propel_WorkshopApp.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 2/24/23.
//

import SwiftUI

@main
public struct Async_Await_Propel_WorkshopApp: App {
    public let dependencyResolver: DependencyResolver = .init()
       
    public init() {}
    
    public var body: some Scene {
        WindowGroup {
            //ContentView(viewModel: DataTestViewModel(dependencyResolver: dependencyResolver))
            Dashboard(searchBarViewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
        }
    }
}


//
//  RequestMaking.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 2/28/23.
//

import Foundation

public protocol RequestMaking {
    func makeRequest() throws -> URLRequest
}

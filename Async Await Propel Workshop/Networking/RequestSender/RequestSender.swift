//
//  RequestSender.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 2/28/23.
//

import Foundation

public class RequestSender: RequestSending {
    private let urlSession: URLSession
    
    public init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }
    
    public func sendRequest<Model>(requestMaker: RequestMaking) async throws -> Model where Model : Decodable {
        let urlRequest: URLRequest = try requestMaker.makeRequest()
        let (data, response): (Data, URLResponse) = try await urlSession.data(for: urlRequest)
        let _: HTTPURLResponse = try urlRequest.validHTTPResponse(httpResponse: response, data: data)
        return try JSONDecoder().decode(Model.self, from: data)
    }
}

//
//  RequestSending.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 2/28/23.
//

import Foundation

public protocol RequestSending {
    func sendRequest<Model: Decodable>(requestMaker: RequestMaking) async throws -> Model
}

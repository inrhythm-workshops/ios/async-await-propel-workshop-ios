//
//  URLComponentGetting.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/18/23.
//

import Foundation

public protocol URLComponentGetting: RawRepresentable {
    var components: URLComponents { get }
}

//
//  EnvironmentGetting.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/18/23.
//

import Foundation

public protocol ServerEnvironmentGetting {
    func scheme() -> String
}

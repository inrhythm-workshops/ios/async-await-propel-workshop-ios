//
//  YahooFinanceURL.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/18/23.
//

import Foundation

public enum YahooFinanceURL: String, CaseIterable, URLComponentGetting {
    enum Constants {
        static let quoteSummaryPath: String = "/stock/v2/get-summary"
        static let quoteTopHoldingsPath: String = "/stock/get-top-holdings"
    }
    case quoteSummary = "QUOTE_SUMMARY"
    case quoteTopHoldings = "QUOTE_TOP_HOLDINGS"
    
    public var components: URLComponents {
        switch self {
        case .quoteSummary:
            return URLComponents(scheme: ServerEnvironment.production.scheme(), hostResource: YahooResource.YFRapidAPIProd.rapidAPIProdHost, path: Constants.quoteSummaryPath)
        case .quoteTopHoldings:
            return URLComponents(scheme: ServerEnvironment.production.scheme(), hostResource: YahooResource.YFRapidAPIProd.rapidAPIProdHost, path: Constants.quoteTopHoldingsPath)
        }
    }
}

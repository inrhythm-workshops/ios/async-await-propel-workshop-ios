//
//  YahooResource.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/17/23.
//

import Foundation

public enum YahooResource: String, CaseIterable {
    case YFRapidAPIProd = "YFRapidAPIProd"
    
    var rapidAPIProdHost: String {
        switch self {
        case .YFRapidAPIProd:
            return "yh-finance.p.rapidapi.com"
        }
    }
    
    var rapidAPIProdAuth: [String: String] {
        switch self {
        case .YFRapidAPIProd:
            return [
                "X-RapidAPI-Key": "68e5bb37c0msh21358b932a621d1p17c89bjsn4b2d51e11191",
                "X-RapidAPI-Host": "yh-finance.p.rapidapi.com"]
        }
    }
}

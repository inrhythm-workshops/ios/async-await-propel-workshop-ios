//
//  File.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/11/23.
//

import Foundation

public struct YahooFinance {
    public enum Constants {
        static let quoteSummaryResource = "https://yh-finance.p.rapidapi.com/stock/v2/get-summary"
        static let quoteRecommendationsResource = "https://yh-finance.p.rapidapi.com/stock/v2/get-recommendations"
        static let quoteTopHoldingsResource = "https://yh-finance.p.rapidapi.com/stock/get-top-holdings"
        static let quoteBalanceSheetResource = "https://yh-finance.p.rapidapi.com/stock/v2/get-balance-sheet"
        static let quoteUpgradeDowngradeResource = "https://yh-finance.p.rapidapi.com/stock/v3/get-upgrades-downgrades"
        static let APIKeyName: String =
        "X-RapidAPI-Key"
        static let APIKeyValue: String = "68e5bb37c0msh21358b932a621d1p17c89bjsn4b2d51e11191"
        static let HostKey: String = "X-RapidAPI-Host"
        static let HostValue: String = "yh-finance.p.rapidapi.com"
    }
    
    static let shared: YahooFinance = YahooFinance()
        
    public var quoteSummary: String  {
        return Constants.quoteSummaryResource
    }
    
    public var quoteRecommendations: String  {
        return Constants.quoteRecommendationsResource
    }
    
    public var quoteTopHoldings: String  {
        return Constants.quoteTopHoldingsResource
    }
    
    public var quoteUpgradeDownGrade: String  {
        return Constants.quoteUpgradeDowngradeResource
    }
    
    public var APIKeyName: String {
        return Constants.APIKeyName
    }
    
    public var APIKeyValue: String {
        return Constants.APIKeyValue
    }
    
    public var APIHostKey: String {
        return Constants.HostKey
    }
    
    public var APIHostValue: String {
        return Constants.HostValue
    }
}



//
//  ServerEnvironment.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/18/23.
//

import Foundation


public enum ServerEnvironment: String, ServerEnvironmentGetting {
    enum Constants {
        static let https: String = "https"
    }
    
    case production
    
    public func scheme() -> String {
        switch self {
        case .production:
            return Constants.https
        }
    }
}

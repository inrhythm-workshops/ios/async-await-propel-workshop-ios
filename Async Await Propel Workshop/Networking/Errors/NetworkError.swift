//
//  NetworkError.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/11/23.
//

import Foundation

public enum NetworkError: Error {
    case badRequest(reason: String?)
    case invalidRequestURL(reason: String?)
    case httpResponseMissing(reason: String?)
    case requestBodyNotJsonEncodable(reason: String?)
}

//
//  URLRequestExtension.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/11/23.
//

import Foundation

extension URLRequest {
    internal func validHTTPResponse(httpResponse: URLResponse, data: Data?) throws -> HTTPURLResponse {
        guard let httpResponse: HTTPURLResponse  = httpResponse as? HTTPURLResponse else {
            throw NetworkError.httpResponseMissing(reason: httpResponse.url?.absoluteString)
        }
        if  httpResponse.statusCode == 200, let data: Data = data, !data.isEmpty {
            return httpResponse
        }
        else {
            throw NetworkError.badRequest(reason: httpResponse.debugDescription)
        }
    }
}

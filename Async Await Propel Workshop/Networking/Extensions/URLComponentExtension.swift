//
//  URLComponentExtension.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/17/23.
//

import Foundation

public extension URLComponents {
    init(scheme: String, hostResource: String, path: String, queryItems: [URLQueryItem]? = nil) {
        var components: URLComponents = URLComponents()
        components.scheme = scheme
        components.host = hostResource
        components.path = path
        components.queryItems = queryItems
        self = components
    }
}

//
//  TopHoldings.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 2/28/23.
//

import Foundation

// MARK: - TopHoldingsResponse
public struct TopHoldingsResponse: Codable {
    let quoteSummary: QuoteSummary?
}

// MARK: - QuoteSummary
public struct QuoteSummary: Codable {
    let result: [Result]?
    let error: String?
}

// MARK: - Result
public struct Result: Codable {
    let topHoldings: TopHoldings?
}

// MARK: - TopHoldings
public struct TopHoldings: Codable {
    let maxAge: Int?
    let cashPosition: CashPosition?
    let stockPosition, bondPosition: BondPosition?
    let otherPosition, preferredPosition, convertiblePosition: CashPosition?
    let holdings: [Holding]?
    let equityHoldings: EquityHoldings?
    let bondHoldings: BondHoldings?
    let bondRatings: [BondRating]?
    let sectorWeightings: [SectorWeighting]?
}

// MARK: - BondHoldings
public struct BondHoldings: Codable {
    let maturity, duration, creditQuality, maturityCat: CashPosition?
    let durationCat, creditQualityCat: CashPosition?
}

// MARK: - CashPosition
public struct CashPosition: Codable {
}

// MARK: - BondPosition
public struct BondPosition: Codable {
    let raw: Double?
    let fmt: String?
}

// MARK: - BondRating
public struct BondRating: Codable {
    let usGovernment: BondPosition?

    enum CodingKeys: String, CodingKey {
        case usGovernment = "us_government"
    }
}

// MARK: - EquityHoldings
public struct EquityHoldings: Codable {
    let priceToEarnings, priceToBook, priceToSales, priceToCashflow: BondPosition?
    let medianMarketCap, threeYearEarningsGrowth, priceToEarningsCat, priceToBookCat: CashPosition?
    let priceToSalesCat, priceToCashflowCat, medianMarketCapCat, threeYearEarningsGrowthCat: CashPosition?
}

// MARK: - Holding
public struct Holding: Codable, Identifiable {
    let symbol, holdingName: String
    let holdingPercent: BondPosition?
    public var id: String {
        self.holdingName
    }
}

// MARK: - SectorWeighting
public struct SectorWeighting: Codable {
    let realestate, consumerCyclical, basicMaterials, consumerDefensive: BondPosition?
    let technology, communicationServices, financialServices, utilities: BondPosition?
    let industrials, energy, healthcare: BondPosition?

    enum CodingKeys: String, CodingKey {
        case realestate
        case consumerCyclical = "consumer_cyclical"
        case basicMaterials = "basic_materials"
        case consumerDefensive = "consumer_defensive"
        case technology
        case communicationServices = "communication_services"
        case financialServices = "financial_services"
        case utilities, industrials, energy, healthcare
    }
}

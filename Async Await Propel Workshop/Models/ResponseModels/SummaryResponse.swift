//
//  SummaryResponse.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/11/23.
//

import Foundation


// MARK: - SummaryResponse
public struct SummaryResponse: Codable {
    let defaultKeyStatistics: DefaultKeyStatistics?
    let details: Details?
    let summaryProfile: SummaryProfile?
    let recommendationTrend: RecommendationTrend?
    let financialsTemplate: FinancialsTemplate?
    let majorDirectHolders: Holders?
    let earnings: WelcomeEarnings?
    let price: Price?
    let fundOwnership: Ownership?
    let insiderTransactions: InsiderTransactions?
    let insiderHolders: Holders?
    let netSharePurchaseActivity: NetSharePurchaseActivity?
    let majorHoldersBreakdown: MajorHoldersBreakdown?
    let financialData: FinancialData?
    let quoteType: QuoteType?
    let institutionOwnership: Ownership?
    let calendarEvents: CalendarEvents?
    let summaryDetail: SummaryDetail?
    let symbol: String?
    let esgScores: EsgScores?
    let upgradeDowngradeHistory: UpgradeDowngradeHistory?
    let pageViews: PageViews?
}

// MARK: - CalendarEvents
struct CalendarEvents: Codable {
    let maxAge: Int?
    let earnings: CalendarEventsEarnings?
    let exDividendDate, dividendDate: Details?
}

// MARK: - Details
struct Details: Codable {
}

// MARK: - CalendarEventsEarnings
struct CalendarEventsEarnings: Codable {
    let earningsDate: [The52_WeekChange]?
    let earningsAverage, earningsLow, earningsHigh: The52_WeekChange?
    let revenueAverage, revenueLow, revenueHigh: EnterpriseValue?
}

// MARK: - The52_WeekChange
struct The52_WeekChange: Codable {
    let raw: Double?
    let fmt: String?
}

// MARK: - EnterpriseValue
struct EnterpriseValue: Codable {
    let raw: Int?
    let fmt: String?
    let longFmt: String?
}

// MARK: - DefaultKeyStatistics
struct DefaultKeyStatistics: Codable {
    let annualHoldingsTurnover: Details?
    let enterpriseToRevenue: The52_WeekChange?
    let beta3Year: Details?
    let profitMargins, enterpriseToEbitda, the52WeekChange: The52_WeekChange?
    let morningStarRiskRating: Details?
    let forwardEps: The52_WeekChange?
    let revenueQuarterlyGrowth: Details?
    let sharesOutstanding: EnterpriseValue?
    let fundInceptionDate, annualReportExpenseRatio, totalAssets: Details?
    let bookValue: The52_WeekChange?
    let sharesShort: EnterpriseValue?
    let sharesPercentSharesOut: The52_WeekChange?
    let fundFamily: String?
    let lastFiscalYearEnd, heldPercentInstitutions: The52_WeekChange?
    let netIncomeToCommon: EnterpriseValue?
    let trailingEps: The52_WeekChange?
    let lastDividendValue: Details?
    let sandP52WeekChange, priceToBook, heldPercentInsiders, nextFiscalYearEnd: The52_WeekChange?
    let yield: Details
    let mostRecentQuarter, shortRatio, sharesShortPreviousMonthDate: The52_WeekChange?
    let floatShares: EnterpriseValue?
    let beta: The52_WeekChange?
    let enterpriseValue, priceHint: EnterpriseValue?
    let threeYearAverageReturn: Details?
    let lastSplitDate: The52_WeekChange?
    let lastSplitFactor: String?
    let legalType: String?
    let lastDividendDate, morningStarOverallRating: Details?
    let earningsQuarterlyGrowth: The52_WeekChange?
    let priceToSalesTrailing12Months: Details?
    let dateShortInterest, pegRatio: The52_WeekChange?
    let ytdReturn: Details?
    let forwardPE: The52_WeekChange?
    let maxAge: Int?
    let lastCapGain: Details?
    let shortPercentOfFloat: The52_WeekChange?
    let sharesShortPriorMonth, impliedSharesOutstanding: EnterpriseValue?
    let category: String?
    let fiveYearAverageReturn: Details?

    enum CodingKeys: String, CodingKey {
        case annualHoldingsTurnover, enterpriseToRevenue, beta3Year, profitMargins, enterpriseToEbitda
        case the52WeekChange = "52WeekChange"
        case morningStarRiskRating, forwardEps, revenueQuarterlyGrowth, sharesOutstanding, fundInceptionDate, annualReportExpenseRatio, totalAssets, bookValue, sharesShort, sharesPercentSharesOut, fundFamily, lastFiscalYearEnd, heldPercentInstitutions, netIncomeToCommon, trailingEps, lastDividendValue
        case sandP52WeekChange = "SandP52WeekChange"
        case priceToBook, heldPercentInsiders, nextFiscalYearEnd, yield, mostRecentQuarter, shortRatio, sharesShortPreviousMonthDate, floatShares, beta, enterpriseValue, priceHint, threeYearAverageReturn, lastSplitDate, lastSplitFactor, legalType, lastDividendDate, morningStarOverallRating, earningsQuarterlyGrowth, priceToSalesTrailing12Months, dateShortInterest, pegRatio, ytdReturn, forwardPE, maxAge, lastCapGain, shortPercentOfFloat, sharesShortPriorMonth, impliedSharesOutstanding, category, fiveYearAverageReturn
    }
}

// MARK: - WelcomeEarnings
struct WelcomeEarnings: Codable {
    let maxAge: Int?
    let earningsChart: EarningsChart?
    let financialsChart: FinancialsChart?
    let financialCurrency: String?
}

// MARK: - EarningsChart
struct EarningsChart: Codable {
    let quarterly: [EarningsChartQuarterly]?
    let currentQuarterEstimate: The52_WeekChange?
    let currentQuarterEstimateDate: String?
    let currentQuarterEstimateYear: Int?
    let earningsDate: [The52_WeekChange]?
}

// MARK: - EarningsChartQuarterly
struct EarningsChartQuarterly: Codable {
    let date: String?
    let actual, estimate: The52_WeekChange?
}

// MARK: - FinancialsChart
struct FinancialsChart: Codable {
    let yearly: [Yearly]?
    let quarterly: [FinancialsChartQuarterly]?
}

// MARK: - FinancialsChartQuarterly
struct FinancialsChartQuarterly: Codable {
    let date: String?
    let revenue, earnings: EnterpriseValue?
}

// MARK: - Yearly
struct Yearly: Codable {
    let date: Int?
    let revenue, earnings: EnterpriseValue?
}

// MARK: - EsgScores
struct EsgScores: Codable {
    let palmOil: Bool?
    let peerSocialPerformance: PeerPerformance?
    let controversialWeapons: Bool?
    let ratingMonth: Int?
    let gambling: Bool?
    let socialScore: The52_WeekChange?
    let nuclear, furLeather, alcoholic, gmo: Bool?
    let catholic: Bool?
    let socialPercentile: String?
    let peerGovernancePerformance: PeerPerformance?
    let peerCount: Int?
    let relatedControversy: [String]?
    let governanceScore: The52_WeekChange?
    let environmentPercentile: String?
    let animalTesting: Bool?
    let peerEsgScorePerformance: PeerPerformance?
    let tobacco: Bool?
    let totalEsg: The52_WeekChange?
    let highestControversy: Int?
    let esgPerformance: String?
    let coal: Bool?
    let peerHighestControversyPerformance: PeerPerformance?
    let pesticides, adult: Bool?
    let ratingYear, maxAge: Int?
    let percentile: The52_WeekChange?
    let peerGroup: String?
    let smallArms: Bool?
    let peerEnvironmentPerformance: PeerPerformance?
    let environmentScore: The52_WeekChange?
    let governancePercentile: String?
    let militaryContract: Bool?
}

// MARK: - PeerPerformance
struct PeerPerformance: Codable {
    let min, avg, max: Double?
}

// MARK: - FinancialData
struct FinancialData: Codable {
    let ebitdaMargins, profitMargins, grossMargins: The52_WeekChange?
    let operatingCashflow: EnterpriseValue?
    let revenueGrowth, operatingMargins: The52_WeekChange?
    let ebitda: EnterpriseValue?
    let targetLowPrice: The52_WeekChange?
    let recommendationKey: String?
    let grossProfits, freeCashflow: EnterpriseValue?
    let targetMedianPrice, currentPrice, earningsGrowth, currentRatio: The52_WeekChange?
    let returnOnAssets: The52_WeekChange?
    let numberOfAnalystOpinions: EnterpriseValue?
    let targetMeanPrice, debtToEquity, returnOnEquity, targetHighPrice: The52_WeekChange?
    let totalCash, totalDebt, totalRevenue: EnterpriseValue?
    let totalCashPerShare: The52_WeekChange?
    let financialCurrency: String?
    let maxAge: Int?
    let revenuePerShare, quickRatio, recommendationMean: The52_WeekChange?
}

// MARK: - FinancialsTemplate
struct FinancialsTemplate: Codable {
    let code: String?
    let maxAge: Int?
}

// MARK: - Ownership
struct Ownership: Codable {
    let maxAge: Int?
    let ownershipList: [OwnershipList]?
}

// MARK: - OwnershipList
struct OwnershipList: Codable {
    let maxAge: Int?
    let reportDate: The52_WeekChange?
    let organization: String?
    let pctHeld: The52_WeekChange?
    let position, value: EnterpriseValue?
    let pctChange: The52_WeekChange?
}

// MARK: - Holders
struct Holders: Codable {
    let holders: [Holder]?
    let maxAge: Int?
}

// MARK: - Holder
struct Holder: Codable {
    let maxAge: Int?
    let name: String?
    let relation: String?
    let url: String?
    let transactionDescription: String?
    let latestTransDate: The52_WeekChange?
    let positionDirect: EnterpriseValue?
    let positionDirectDate: The52_WeekChange?
    let positionIndirect: EnterpriseValue?
    let positionIndirectDate: The52_WeekChange?
}

// MARK: - InsiderTransactions
struct InsiderTransactions: Codable {
    let transactions: [Transaction]?
    let maxAge: Int?
}

// MARK: - Transaction
struct Transaction: Codable {
    let filerName: String?
    let transactionText, moneyText: String?
    let ownership: String?
    let startDate: The52_WeekChange?
    let value: EnterpriseValue?
    let filerRelation: String?
    let shares: EnterpriseValue?
    let filerURL: String?
    let maxAge: Int?

    enum CodingKeys: String, CodingKey {
        case filerName, transactionText, moneyText, ownership, startDate, value, filerRelation, shares
        case filerURL = "filerUrl"
        case maxAge
    }
}

// MARK: - MajorHoldersBreakdown
struct MajorHoldersBreakdown: Codable {
    let maxAge: Int?
    let insidersPercentHeld, institutionsPercentHeld, institutionsFloatPercentHeld: The52_WeekChange?
    let institutionsCount: EnterpriseValue?
}

// MARK: - NetSharePurchaseActivity
struct NetSharePurchaseActivity: Codable {
    let period: String
    let netPercentInsiderShares: The52_WeekChange?
    let netInfoCount, totalInsiderShares, buyInfoShares: EnterpriseValue?
    let buyPercentInsiderShares: The52_WeekChange?
    let sellInfoCount, sellInfoShares: EnterpriseValue?
    let sellPercentInsiderShares: The52_WeekChange?
    let maxAge: Int?
    let buyInfoCount, netInfoShares: EnterpriseValue?
}

// MARK: - PageViews
struct PageViews: Codable {
    let shortTermTrend, midTermTrend, longTermTrend: String?
    let maxAge: Int?
}

// MARK: - Price
struct Price: Codable {
    let quoteSourceName: String?
    let regularMarketOpen: The52_WeekChange?
    let averageDailyVolume3Month: EnterpriseValue?
    let exchange: String?
    let regularMarketTime: Int?
    let volume24Hr: Details?
    let regularMarketDayHigh: The52_WeekChange?
    let shortName: String?
    let averageDailyVolume10Day: EnterpriseValue?
    let longName: String?
    let regularMarketChange: The52_WeekChange?
    let currencySymbol: String?
    let regularMarketPreviousClose: The52_WeekChange?
    let postMarketTime: Int?
    let preMarketPrice: Details?
    let exchangeDataDelayedBy: Int?
    let toCurrency: String?
    let postMarketChange, postMarketPrice: The52_WeekChange?
    let exchangeName: String?
    let preMarketChange, circulatingSupply: Details?
    let regularMarketDayLow: The52_WeekChange?
    let priceHint: EnterpriseValue?
    let currency: String?
    let regularMarketPrice: The52_WeekChange?
    let regularMarketVolume: EnterpriseValue?
    let lastMarket: String?
    let regularMarketSource: String?
    let openInterest: Details?
    let marketState: String?
    let underlyingSymbol: String?
    let marketCap: EnterpriseValue?
    let quoteType: String?
    let volumeAllCurrencies: Details?
    let postMarketSource: String?
    let strikePrice: Details?
    let symbol: String?
    let postMarketChangePercent: The52_WeekChange?
    let preMarketSource: String?
    let maxAge: Int?
    let fromCurrency: String?
    let regularMarketChangePercent: The52_WeekChange?
}

// MARK: - QuoteType
struct QuoteType: Codable {
    let exchange, shortName, longName, exchangeTimezoneName: String?
    let exchangeTimezoneShortName: String?
    let isEsgPopulated: Bool?
    let gmtOffSetMilliseconds, quoteType, symbol, messageBoardID: String?
    let market: String?

    enum CodingKeys: String, CodingKey {
        case exchange, shortName, longName, exchangeTimezoneName, exchangeTimezoneShortName, isEsgPopulated, gmtOffSetMilliseconds, quoteType, symbol
        case messageBoardID = "messageBoardId"
        case market
    }
}

// MARK: - RecommendationTrend
struct RecommendationTrend: Codable {
    let trend: [Trend]?
    let maxAge: Int?
}

// MARK: - Trend
struct Trend: Codable {
    let period: String?
    let strongBuy, buy, hold, sell: Int?
    let strongSell: Int?
}

// MARK: - SummaryDetail
struct SummaryDetail: Codable {
    let previousClose, regularMarketOpen, twoHundredDayAverage, trailingAnnualDividendYield: The52_WeekChange?
    let payoutRatio: The52_WeekChange?
    let volume24Hr: Details?
    let regularMarketDayHigh: The52_WeekChange?
    let navPrice: Details?
    let averageDailyVolume10Day: EnterpriseValue?
    let totalAssets: Details?
    let regularMarketPreviousClose, fiftyDayAverage, trailingAnnualDividendRate, summaryDetailOpen: The52_WeekChange?
    let toCurrency: String?
    let averageVolume10Days: EnterpriseValue?
    let expireDate, yield: Details?
    let algorithm: String?
    let dividendRate, exDividendDate: Details?
    let beta: The52_WeekChange?
    let circulatingSupply, startDate: Details?
    let regularMarketDayLow: The52_WeekChange?
    let priceHint: EnterpriseValue?
    let currency: String?
    let trailingPE: The52_WeekChange?
    let regularMarketVolume: EnterpriseValue?
    let lastMarket: String?
    let maxSupply, openInterest: Details?
    let marketCap: EnterpriseValue?
    let volumeAllCurrencies, strikePrice: Details?
    let averageVolume: EnterpriseValue?
    let priceToSalesTrailing12Months, dayLow, ask: The52_WeekChange?
    let ytdReturn: Details?
    let askSize, volume: EnterpriseValue?
    let fiftyTwoWeekHigh, forwardPE: The52_WeekChange?
    let maxAge: Int?
    let fromCurrency: String?
    let fiveYearAvgDividendYield: Details?
    let fiftyTwoWeekLow, bid: The52_WeekChange?
    let tradeable: Bool?
    let dividendYield: Details?
    let bidSize: EnterpriseValue?
    let dayHigh: The52_WeekChange?
    let coinMarketCapLink: String?

    enum CodingKeys: String, CodingKey {
        case previousClose, regularMarketOpen, twoHundredDayAverage, trailingAnnualDividendYield, payoutRatio, volume24Hr, regularMarketDayHigh, navPrice, averageDailyVolume10Day, totalAssets, regularMarketPreviousClose, fiftyDayAverage, trailingAnnualDividendRate
        case summaryDetailOpen = "open"
        case toCurrency
        case averageVolume10Days = "averageVolume10days"
        case expireDate, yield, algorithm, dividendRate, exDividendDate, beta, circulatingSupply, startDate, regularMarketDayLow, priceHint, currency, trailingPE, regularMarketVolume, lastMarket, maxSupply, openInterest, marketCap, volumeAllCurrencies, strikePrice, averageVolume, priceToSalesTrailing12Months, dayLow, ask, ytdReturn, askSize, volume, fiftyTwoWeekHigh, forwardPE, maxAge, fromCurrency, fiveYearAvgDividendYield, fiftyTwoWeekLow, bid, tradeable, dividendYield, bidSize, dayHigh, coinMarketCapLink
    }
}

// MARK: - SummaryProfile
struct SummaryProfile: Codable {
    let zip, sector: String?
    let fullTimeEmployees: Int?
    let longBusinessSummary, city, phone, state: String?
    let country: String?
    let companyOfficers: [String]?
    let website: String?
    let maxAge: Int?
    let address1, industry: String?
}

// MARK: - UpgradeDowngradeHistory
struct UpgradeDowngradeHistory: Codable {
    let history: [History]?
    let maxAge: Int?
}

// MARK: - History
struct History: Codable {
    let epochGradeDate: Int?
    let firm: String?
    let toGrade, fromGrade: String?
    let action: String?
}

//
//  TestDependencyResolver.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

public class TestDependencyResolver: TestDependencyResolving {
    
    public func resolveYFSummarFetching() -> YFQuoteSummaryFetching {
        TestQuoteSummaryFetcher(dependencyResolver: self)
    }
    
    public func resolveYFTopHoldingsFetching() -> YFQuoteTopHoldingsFetching {
        TestQuoteTopHoldingsFetcher(dependencyResolver: self)
    }
    
    public func resolveRequestSending() -> RequestSending {
        RequestSender()
    }
}

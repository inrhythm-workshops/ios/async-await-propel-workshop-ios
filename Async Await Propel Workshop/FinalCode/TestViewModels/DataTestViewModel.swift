//
//  DataTestViewModel.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

enum LoadState {
    case loaded
    case loading
    case error
}
public class DataTestViewModel: ObservableObject {
    @Published var quoteSummary: SummaryResponse?
    @Published var topHoldings: TopHoldingsResponse?
    private let dataFetcher: YFQuoteSummaryFetching
    private let dataFetcherForHoldings: YFQuoteTopHoldingsFetching
    private let dependencyResolver: TestDependencyResolving
    @Published var loadState: LoadState = .loaded
    
    init(dependencyResolver: TestDependencyResolving) {
        self.dependencyResolver = dependencyResolver
        self.dataFetcher = TestQuoteSummaryFetcher(dependencyResolver: dependencyResolver)
        self.dataFetcherForHoldings = TestQuoteTopHoldingsFetcher(dependencyResolver: dependencyResolver)
    }
    
    @MainActor
    func fetchQuoteSummary(symbolValue: String = "META") { // first  
        self.loadState = .loading
        Task {
            do {
                self.quoteSummary =   try await self.dataFetcher.fetchSummary(symbolValue: symbolValue)
                self.loadState = .loaded
            } catch let error {
                self.loadState = .error
                print("Error: \(error.localizedDescription.debugDescription)")
            }
        }
    }
    
    @MainActor
    func fetchTopHoldings(symbolValue: String = "SPY") {
        self.loadState = .loading
        Task {
            do {
                self.topHoldings =   try await self.dataFetcherForHoldings.fetchTopHoldings(symbolValue: symbolValue)
                self.loadState = .loaded
            } catch let error {
                self.loadState = .error
                print("Error: \(error.localizedDescription.debugDescription)")
            }
        }
    }
}

//
//  TopHoldingsTestDataFetcher.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

public class TestQuoteTopHoldingsFetcher: YFQuoteTopHoldingsFetching {
    private var dependencyResolver: TestDependencyResolving
    private var requestSender: RequestSending
    
    init(dependencyResolver: TestDependencyResolving) {
        self.dependencyResolver = dependencyResolver
        self.requestSender = dependencyResolver.resolveRequestSending()
    }
    
   public  func fetchTopHoldings(symbolValue: String) async throws -> TopHoldingsResponse {
        let requestMaker: RequestMaker
        do {
            requestMaker = try makeQuoteTopHoldingsRequest(symbolValue: symbolValue)
        }
        catch let error {
            throw YahooFinanceServiceErrors.YFQuoteTopHoldingsRequestMaker(reason: error.localizedDescription.debugDescription)
        }
        do {
            let topHoldingsResponse: TopHoldingsResponse = try await requestSender.sendRequest(requestMaker: requestMaker)
            print("HoldingDate: \(topHoldingsResponse)")
            return topHoldingsResponse
        }
        catch let error {
            throw YahooFinanceServiceErrors.YFQuoteTopHoldingsFetching(reason: error.localizedDescription.debugDescription)
        }
    }
    
    // For top holdings, remember we need to make request for an ETF or Index fund and not the Equity(stock) since a single does not hold any Top Holdings. I will recomend pick "SPY" which is en ETF
    func makeQuoteTopHoldingsRequest(symbolValue: String) throws -> RequestMaker {
        var urlComponent = YahooFinanceURL.quoteTopHoldings.components
        let queryItem: URLQueryItem = URLQueryItem(name: "symbol", value: symbolValue)
        urlComponent.queryItems = [queryItem]
        let requestMaker: RequestMaker = RequestMaker(urlComponent: urlComponent)
        requestMaker.assign(httpRequestMethod: .get)
        requestMaker.assign(headers: YahooResource.YFRapidAPIProd.rapidAPIProdAuth)
        return requestMaker
    }
}

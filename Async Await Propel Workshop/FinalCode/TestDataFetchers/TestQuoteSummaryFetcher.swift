//
//  SummaryTestDataFetcher.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import Foundation

class TestQuoteSummaryFetcher: YFQuoteSummaryFetching {
    private var dependencyResolver: TestDependencyResolving
    private var requestSender: RequestSending
    
    init(dependencyResolver: TestDependencyResolving) {
        self.dependencyResolver = dependencyResolver
        self.requestSender = dependencyResolver.resolveRequestSending()
    }
    
    func fetchSummary(symbolValue: String) async throws -> SummaryResponse {
        let requestMaker: RequestMaker
        requestMaker = makeQuoteSummaryRequest(symbolValue: symbolValue)
        
        do {
            let summaryResponse: SummaryResponse = try await requestSender.sendRequest(requestMaker: requestMaker)
            return summaryResponse
        }
        catch let error {
            throw YahooFinanceServiceErrors.YFQuoteSummaryFetching(reason: error.localizedDescription)
           
        }
    }
    
    func makeQuoteSummaryRequest(symbolValue: String) -> RequestMaker {
        var urlComponent = YahooFinanceURL.quoteSummary.components
        let queryItem: URLQueryItem = URLQueryItem(name: "symbol", value: symbolValue)
        urlComponent.queryItems = [queryItem]
        let requestMaker: RequestMaker = RequestMaker(urlComponent: urlComponent)
        requestMaker.assign(httpRequestMethod: .get)
        requestMaker.assign(headers: YahooResource.YFRapidAPIProd.rapidAPIProdAuth)
        return requestMaker
    }
}

//
//  DateTest.swift
//  Async Await Propel Workshop
//
//  Created by Hamid Mahmood on 3/12/23.
//

import SwiftUI

public struct DataTestView: View {
    @ObservedObject var viewModel: SearchBarViewModel
    
    public var body: some View {
        VStack {
            switch viewModel.loadState {
            case .loading:
                ProgressView("Loading")
            case .loaded:
                Text(viewModel.quoteSummary?.summaryProfile?.sector ?? "")
                Text(viewModel.quoteSummary?.summaryProfile?.address1 ?? "")
                Text(viewModel.quoteSummary?.summaryProfile?.city ?? "")
                Text(viewModel.quoteSummary?.summaryProfile?.country ?? "")
                Text(viewModel.quoteSummary?.summaryProfile?.longBusinessSummary ?? "no data")
                //                Text(String(viewModel.topHoldings?.quoteSummary.result.count ?? 0))
            case .error:
                Text("Can't fetch data")
            }
        }
        .onAppear{
            viewModel.fetchQuoteSummary()
            viewModel.fetchTopHoldings()
        }
    }
}

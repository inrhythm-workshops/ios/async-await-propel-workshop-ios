//
//  DetailPageView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI


struct DetailPageView: View {
    
//    @Binding var tickerInfo: TickerInfo
//    @Binding var changeInfo: ChangeInfo
//    @Binding var topHoldingsContent: String
//    @Binding var upgradeDowngradeContent: String
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        VStack(spacing: 0) {
            switch viewModel.loadState {
            case .loading:
                ProgressView("Loading")
            case .error:
                Text("Can't fetch data")
            case .loaded:
            HStack {
                Spacer()
                Text(viewModel.searchValue)
                Spacer()
            }
            TickerView(viewModel: viewModel)
            HStack(spacing: 0) {
                ChangeView(viewModel: viewModel)
               // SmallDetailsView(viewModel: viewModel)
            }
            SummaryView(viewModel: viewModel)
            HStack(spacing: 0){
                TopHoldingsView(viewModel: viewModel)
                UpgradesDowngradesView(viewModel: viewModel)
            }
            .padding()
            }
        }
        .background(Color(hex: 0xEFEFF6, alpha: 1.0))
        .padding()
            .onAppear(){
             viewModel.fetchQuoteSummary()
              viewModel.fetchTopHoldings()
            }
    }
}

struct DetailPageView_Preview: PreviewProvider {

    static var previews: some View {
        VStack{
            let dependencyResolver: DependencyResolver = .init()
            DetailPageView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
        }
    }
}

//
//  TopHoldingsView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct TopHoldingsView: View {
    //@Binding var topHoldingsContent: String
    @State private var isShowingHoldingView = false
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        HStack{
            Button(action: {
                isShowingHoldingView = true
            }, label: {
                if let holdingsCount = viewModel.topHoldings?.quoteSummary?.result?.first?.topHoldings?.holdings?.count {
                    Text("Top Holdings - \(holdingsCount)")
                        .foregroundColor(Color.white)
                }
            })
            .padding()
        }
        .navigationDestination(isPresented: $isShowingHoldingView) {
            AdditionalDetailsView(viewModel: viewModel)
        }
        .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
        .border(.white)
        .background(Color(hex: 0x339FFF, alpha: 1.0))
        .cornerRadius(10)
    }
}

struct TopHoldingsView_Preview: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        TopHoldingsView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

//
//  SummaryView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct SummaryView: View {
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        VStack {
            VStack {
                Text(viewModel.quoteSummary?.summaryProfile?.longBusinessSummary ?? "")
            }
            .padding()
        }
        .frame(maxWidth: .infinity, maxHeight: 300)
        .border(.black)
    }
}

struct SummaryView_Preview: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        SummaryView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

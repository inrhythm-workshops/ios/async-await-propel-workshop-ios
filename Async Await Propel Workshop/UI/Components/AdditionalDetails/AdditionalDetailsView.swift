//
//  AdditionalDetailsView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct AdditionalDetailsView: View {
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        VStack(spacing: 0) {
            Section {
                DetailsHeader()
            }
            .padding()
            .background(Color.white)
            
            VStack {
                if let holdings = viewModel.topHoldings?.quoteSummary?.result?.first?.topHoldings?.holdings{
                    List(holdings) { holding in
                        HStack {
                            Spacer()
                            Text(holding.symbol)
                            if let percentage = holding.holdingPercent?.fmt {
                                Spacer()
                                Text("\(percentage)")
                                Spacer()
                            }
                        }
                    }
                }
                Spacer()
            }
        }
        .background(Color(hex: 0xEFEFF6, alpha: 1.0))
        Spacer()
    }
}

struct AdditionalDetails_Preview: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        AdditionalDetailsView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

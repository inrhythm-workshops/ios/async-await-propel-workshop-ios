//
//  TickerView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct TickerInfo {
    var symbolImage: Image?
    var symbolText: String
    var market: String
    var open: String
    var low: String
    var high: String
}

struct TickerView: View {
 
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        VStack {
            HStack{
                VStack(alignment: .center) {
                    Image(systemName: "questionmark.folder.fill")
                }
                VStack(alignment: .center) {
                    Text("Symbol")
                        .font(.system(size: 12))
                        .foregroundColor(.gray)
                    Text(viewModel.quoteSummary?.symbol ?? "")
                }
                Divider()
                VStack(alignment: .center) {
                    Text("Market")
                        .font(.system(size: 12))
                        .foregroundColor(.gray)
                    Text(viewModel.quoteSummary?.fundOwnership?.ownershipList?.first?.organization ?? "")
                }
                Divider()
                VStack(alignment: .center) {
                    Text("Open")
                        .font(.system(size: 12))
                        .foregroundColor(.gray)
                    let open = "\(viewModel.quoteSummary?.summaryDetail?.summaryDetailOpen?.fmt ?? "")"
                    Text(open)
                }
                Divider()
                VStack(alignment: .center) {
                    Text("High")
                        .font(.system(size: 12))
                        .foregroundColor(.gray)
                    let highPrice = "\(viewModel.quoteSummary?.financialData?.targetHighPrice?.fmt ?? "")"
                    Text(highPrice)
                }
                Divider()
                VStack(alignment: .center) {
                    Text("Low")
                        .font(.system(size: 12))
                        .foregroundColor(.gray)
                    let lowPrice = "$\(viewModel.quoteSummary?.financialData?.targetLowPrice?.fmt ?? "")"
                    Text(lowPrice)
                }
            }
            .frame(maxWidth: .infinity, maxHeight: 40)
            
        }.border(.gray)
    }
}

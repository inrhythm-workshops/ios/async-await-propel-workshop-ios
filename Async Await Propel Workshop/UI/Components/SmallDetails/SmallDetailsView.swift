//
//  SmallDetailsView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct SmallDetailsView: View {
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        HStack() {
            VStack {
                Text("Another Small View")
                Text("To Display Desired Data")
                Text("[View]")
                    .font(.system(size: 12, weight:.bold))
            }
            .padding()
        }
        .frame(maxWidth: .infinity, minHeight: 150, maxHeight: 150)
        .border(.gray)
    }
}

struct SmallDetailsView_Preview: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        SmallDetailsView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

//
//  ChangeView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI


enum ChangeDirection {
    case up, down, none
}
struct ChangeInfo {
    var current: String
    var changeAmount: String
    var changePercent: String
    var changeDirection: ChangeDirection
    var changeColor: Color {
        switch self.changeDirection {
        case .down:
            return .red
        case .up:
            return .green
        case .none:
            return .white
        }
    }
}

struct ChangeView: View {
    //@Binding var changeInfo: ChangeInfo
    @ObservedObject var viewModel: SearchBarViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Spacer()
            let current = "Current Price: $\(viewModel.quoteSummary?.financialData?.currentPrice?.fmt ?? "")"
            Text(current)
            Spacer()
            HStack {
                let changeAmount = "Post Market Change Amount: $\(viewModel.quoteSummary?.price?.postMarketChange?.fmt ?? "")"
                Text(changeAmount)
                Spacer()
                if let changePercent = viewModel.quoteSummary?.price?.postMarketChangePercent?.fmt {
                    Text("\(changePercent)")
                        .foregroundColor((Double(changePercent) ?? 0.00 > 0.00) ? .green : .red )
                }
            }
            Spacer()
        }
        .frame(maxWidth: .infinity, minHeight: 150, maxHeight: 150)
        .border(.gray)
    }
}

struct ChangeView_Preview: PreviewProvider {
    static var previews: some View {
        VStack {
            let dependencyResolver: DependencyResolver = .init()
            ChangeView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
        }
    }
}

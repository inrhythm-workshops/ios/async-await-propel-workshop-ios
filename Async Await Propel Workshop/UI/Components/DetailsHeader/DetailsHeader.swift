//
//  DetailsHeader.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct DetailsHeader: View {
    var body: some View {
        VStack {
            HStack {
                Text("Top Holdings - Details")
            }
            .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
            .border(.gray)
        }
    }
}

struct DetailsHeader_Preview: PreviewProvider {
    static var previews: some View {
        DetailsHeader()
    }
}

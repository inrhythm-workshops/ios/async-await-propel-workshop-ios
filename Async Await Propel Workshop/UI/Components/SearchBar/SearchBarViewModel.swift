//
//  SearchBarViewModel.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/3/23.
//

import Foundation
import SwiftUI

class SearchBarViewModel: ObservableObject {
    var searchValue: String = ""
    @Published var quoteSummary: SummaryResponse?
    @Published var topHoldings: TopHoldingsResponse?
    private let dataFetcher: YFQuoteSummaryFetching
    private let dataFetcherForHoldings: YFQuoteTopHoldingsFetching
    private let dependencyResolver: DependencyResolving
    @Published var loadState: LoadState = .loaded
    
    init(dependencyResolver: DependencyResolving) {
        self.dependencyResolver = dependencyResolver
        self.dataFetcher = YFQuoteSummaryFetcher(dependencyResolver: dependencyResolver)
        self.dataFetcherForHoldings = YFQuoteTopHoldingsFetcher(dependencyResolver: dependencyResolver)
    }
    
    @MainActor
    func fetchQuoteSummary() {
        // TODO: Async/Await code goes here
        fatalError("This method is not implemented yet.")
    }
    
    @MainActor
    func fetchTopHoldings(symbolValue: String = "SPY") {
        self.loadState = .loading
        Task {
            do {
                self.topHoldings =   try await self.dataFetcherForHoldings.fetchTopHoldings(symbolValue: symbolValue)
                self.loadState = .loaded
            } catch let error {
                self.loadState = .error
                print("Error: \(error.localizedDescription.debugDescription)")
            }
        }
    }
}
    


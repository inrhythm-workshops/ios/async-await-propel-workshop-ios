//
//  SearchBar.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/3/23.
//

import Foundation
import SwiftUI

struct SearchBar: View {
    @ObservedObject var searchFieldViewModel: SearchBarViewModel
    
    var body: some View {
        TextField("", text: $searchFieldViewModel.searchValue, prompt: Text("Stock Symbol"))
            .font(.system(size: 20))
            .frame(maxWidth: .infinity, maxHeight: 75, alignment: .leading)
    }
}

struct Previews_SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        SearchBar(searchFieldViewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

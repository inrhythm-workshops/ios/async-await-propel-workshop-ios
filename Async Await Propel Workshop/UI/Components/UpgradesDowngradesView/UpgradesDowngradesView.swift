//
//  UpgradesDowngradesView.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/7/23.
//

import SwiftUI

struct UpgradesDowngradesView: View {
    //@Binding var content: String
    @ObservedObject var viewModel: SearchBarViewModel
    var body: some View {
        HStack{
            Button(action: {
                
            }, label: {
                // here  Text("\(content)")
                Text("Upgrades Button")
                    .foregroundColor(Color.white)
            })
            .padding()
        }
        .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50)
        .border(.white)
        .background(Color(hex: 0x339FFF, alpha: 1.0))
        .cornerRadius(10)
    }
}

struct UpgradesDowngradesView_Preview: PreviewProvider {
    static var previews: some View {
       let dependencyResolver: DependencyResolver = .init()
        UpgradesDowngradesView(viewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}

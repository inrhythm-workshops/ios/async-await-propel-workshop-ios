//
//  Dashboard.swift
//  Async Await Propel Workshop
//
//  Created by Joshua Buchanan on 3/3/23.
//

import Foundation
import SwiftUI

struct Dashboard: View {
    public let dependencyResolver: DependencyResolver = .init()
    @ObservedObject var searchBarViewModel: SearchBarViewModel
    @State private var isShowingDetailView = false
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                Spacer()
                    .frame(height: 140)
                Section {
                    SearchBar(searchFieldViewModel: searchBarViewModel)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(color: Color.gray, radius: 5, x: 0, y: 3)
                .padding()
                
                Spacer()
                    .frame(height: 10)
                Section {
                    SearchButton {
                        isShowingDetailView = true
                    }
                    Spacer()
                }
                .background(Color.clear)
                .cornerRadius(10)
                .padding()
            }
            .background(Color(hex: 0xEFEFF6, alpha: 1.0))
            .padding(.top)
            .ignoresSafeArea()
            .navigationTitle("Dashboard")
            .navigationDestination(isPresented: $isShowingDetailView) {
                DetailPageView(viewModel: searchBarViewModel)
            }
        }
        
    }
}

struct SearchButton: View {
    var action: () -> Void
    var body: some View {
        Button(action: action) {
            Text("Search")
                .frame(maxWidth: .infinity, minHeight: 50)
                .foregroundColor(Color.white)
        }
        .background(Color(hex: 0x339FFF, alpha: 1.0))
    }
}


struct Previews_Dashboard_Previews: PreviewProvider {
    static var previews: some View {
        let dependencyResolver: DependencyResolver = .init()
        Dashboard(searchBarViewModel: SearchBarViewModel(dependencyResolver: dependencyResolver))
    }
}



extension Color {
    init(hex: UInt, alpha: Double = 1) {
        self.init(
            .sRGB,
            red: Double((hex >> 16) & 0xff) / 255,
            green: Double((hex >> 08) & 0xff) / 255,
            blue: Double((hex >> 00) & 0xff) / 255,
            opacity: alpha
        )
    }
}

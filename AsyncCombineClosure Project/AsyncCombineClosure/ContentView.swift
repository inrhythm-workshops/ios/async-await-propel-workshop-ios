//
//  ContentView.swift
//  AsyncCombineClosure
//
//  Created by Michael Long on 3/24/23.
//

import SwiftUI

struct ContentView: View {

     @StateObject var closureViewModel = ContentViewModelClosure()
     @StateObject var combineViewModel = ContentViewModelCombine()
     @StateObject var asyncViewModel = ContentViewModelAsync()

    let food = "cake"
    //let food = "pizza"
    

    var body: some View {
        VStack {
            VStack {
                Text("Closure Results:")
                if let error = closureViewModel.error {
                    Text("Error: \(error)")
                        .foregroundColor(.red)
                } else {
                    VStack(spacing: 20) {
                        if let meals = closureViewModel.results?.meals {
                            Text("\(meals.count) recipies found for \(food).")
                        } else {
                            Text("No meals found.")
                        }
                        if let category = closureViewModel.category, let meals = closureViewModel.resultsByCategory?.meals {
                            Text("\(meals.count) meals found in \(category.lowercased()).")
                        } else {
                            Text("No category found.")
                        }
                    }
                }
            }
            .padding()
            .onAppear {
                closureViewModel.load(food: food)
            }
            
            VStack {
                Text("Combine Results:")
                if let error = combineViewModel.error {
                    Text("Error: \(error)")
                        .foregroundColor(.red)
                } else {
                    VStack(spacing: 20) {
                        if let meals = combineViewModel.results?.meals {
                            Text("\(meals.count) recipies found for \(food).")
                        } else {
                            Text("No meals found.")
                        }
                        if let category = combineViewModel.category, let meals = combineViewModel.resultsByCategory?.meals {
                            Text("\(meals.count) meals found in \(category.lowercased()).")
                        } else {
                            Text("No category found.")
                        }
                    }
                }
            }
            .padding()
            .onAppear {
                combineViewModel.load(food: food)
            }
            
            VStack {
                Text("Async Await Results:")
                if let error = asyncViewModel.error {
                    Text("Error: \(error)")
                        .foregroundColor(.red)
                } else {
                    VStack(spacing: 20) {
                        if let meals = asyncViewModel.results?.meals {
                            Text("\(meals.count) recipies found for \(food).")
                        } else {
                            Text("No meals found.")
                        }
                        if let category = asyncViewModel.category, let meals = asyncViewModel.resultsByCategory?.meals {
                            Text("\(meals.count) meals found in \(category.lowercased()).")
                        } else {
                            Text("No category found.")
                        }
                    }
                }
            }
            .padding()
            .onAppear {
                asyncViewModel.load(food: food)
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

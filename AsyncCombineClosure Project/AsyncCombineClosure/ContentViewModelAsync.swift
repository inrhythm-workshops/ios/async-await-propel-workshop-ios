//
//  ContentViewModelAsync.swift
//  AsyncCombineClosure
//
//  Created by Michael Long on 3/24/23.
//

import Foundation
import Combine

class ContentViewModelAsync: ObservableObject {

    @Published var results: MealsList?
    @Published var category: String?
    @Published var resultsByCategory: MealsList?
    @Published var error: String?

    @MainActor
    func load(food: String) {
        Task {
            do {
                results = try await queryByFood(name: food)
                category = results?.meals.first?.strCategory
                resultsByCategory = try await queryByFood(category: category)
            } catch {
                self.error = error.localizedDescription
            }
        }
    }

    func queryByFood(name: String) async throws -> MealsList {
        let path = "https://www.themealdb.com/api/json/v1/1/search.php?s=\(name)"
        let request = URLRequest(url: URL(string: path)!)
        let (data, _) = try await URLSession.shared.data(for: request)
        return try JSONDecoder().decode(MealsList.self, from: data)
    }

    func queryByFood(category: String?) async throws -> MealsList? {
        guard let category = category else {
            return nil
        }
        let path = "https://www.themealdb.com/api/json/v1/1/filter.php?c=\(category)"
        let request = URLRequest(url: URL(string: path)!)
        let (data, _) = try await URLSession.shared.data(for: request)
        return try JSONDecoder().decode(MealsList.self, from: data)
    }

}

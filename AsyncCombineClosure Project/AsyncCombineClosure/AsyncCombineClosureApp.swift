//
//  AsyncCombineClosureApp.swift
//  AsyncCombineClosure
//
//  Created by Michael Long on 3/24/23.
//

import SwiftUI

@main
struct AsyncCombineClosureApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

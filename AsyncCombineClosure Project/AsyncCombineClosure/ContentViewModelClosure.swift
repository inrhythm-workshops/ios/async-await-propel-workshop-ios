//
//  ContentViewModelClosure.swift
//  AsyncCombineClosure
//
//  Created by Michael Long on 3/24/23.
//

import Foundation

class ContentViewModelClosure: ObservableObject {

    @Published var results: MealsList?
    @Published var category: String?
    @Published var resultsByCategory: MealsList?
    @Published var error: String?

    func load(food: String) {
        queryByFood(name: food) { list, error in
            if let list = list {
                DispatchQueue.main.async {
                    self.results = list
                    self.category = list.meals.first?.strCategory
                    self.error = nil
                    if let category = self.category {
                        self.queryByFood(category: category) { list, error in
                            if let list = list {
                                DispatchQueue.main.async {
                                    self.resultsByCategory = list
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.error = error?.localizedDescription ?? "Unknown Error"
                                }
                            }
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.error = error?.localizedDescription ?? "Unknown Error"
                }
            }
        }
    }

    func queryByFood(name: String, result: @escaping (MealsList?, FoodError?) -> ()) {
        let path = "https://www.themealdb.com/api/json/v1/1/search.php?s=\(name)"
        let request = URLRequest(url: URL(string: path)!)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                if let list = try? JSONDecoder().decode(MealsList.self, from: data) {
                    result(list, nil)
                } else {
                    result(nil, .decoding)
                }
            } else {
                result(nil, .unknown(error?.localizedDescription))
            }
        }
        .resume()
    }

    func queryByFood(category: String, result: @escaping (MealsList?, FoodError?) -> ()) {
        let path = "https://www.themealdb.com/api/json/v1/1/filter.php?c=\(category)"
        let request = URLRequest(url: URL(string: path)!)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                if let list = try? JSONDecoder().decode(MealsList.self, from: data) {
                    result(list, nil)
                } else {
                    result(nil, .decoding)
                }
            } else {
                result(nil, .unknown(error?.localizedDescription))
            }
        }
        .resume()
    }

}

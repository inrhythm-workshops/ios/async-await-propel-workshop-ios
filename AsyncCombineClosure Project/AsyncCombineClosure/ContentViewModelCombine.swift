//
//  ContentViewModelCombine.swift
//  AsyncCombineClosure
//
//  Created by Michael Long on 3/24/23.
//

import Foundation
import Combine

class ContentViewModelCombine: ObservableObject {

    @Published var results: MealsList?
    @Published var category: String?
    @Published var resultsByCategory: MealsList?
    @Published var error: String?

    private var cancellables = Set<AnyCancellable>()

    func load(food: String) {
        queryByFood(name: food)
            .flatMap { (results) -> AnyPublisher<(MealsList, MealsList?), FoodError> in
                if let category = results.meals.first?.strCategory {
                    return self.queryByFood(category: category)
                        .map { (results, $0) }
                        .eraseToAnyPublisher()
                } else {
                    return Just<(MealsList, MealsList?)>((results, nil))
                        .setFailureType(to: FoodError.self)
                        .eraseToAnyPublisher()
                }
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    self.error = error.localizedDescription
                case .finished:
                    self.error = nil
                }
            }, receiveValue: { (results, categorized) in
                self.results = results
                self.resultsByCategory = categorized
                self.category = results.meals.first?.strCategory
            })
            .store(in: &cancellables)
    }

    func queryByFood(name: String) -> AnyPublisher<MealsList, FoodError> {
        let path = "https://www.themealdb.com/api/json/v1/1/search.php?s=\(name)"
        let request = URLRequest(url: URL(string: path)!)
        return URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .decode(type: MealsList.self, decoder: JSONDecoder())
            .mapError { FoodError.unknown($0.localizedDescription) }
            .eraseToAnyPublisher()
    }

    func queryByFood(category: String) -> AnyPublisher<MealsList, FoodError> {
        let path = "https://www.themealdb.com/api/json/v1/1/filter.php?c=\(category)"
        let request = URLRequest(url: URL(string: path)!)
        return URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .decode(type: MealsList.self, decoder: JSONDecoder())
            .mapError { FoodError.unknown($0.localizedDescription) }
            .eraseToAnyPublisher()
    }

}

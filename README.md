# Async Await Propel Workshop




 ### Step 1: Go to the gitlab repo link the link https://gitlab.com/inrhythm-workshops/ios/async-await-propel-workshop-ios . Under "Repository->Branches", choose following branch.
 ``` STARTER-Async-Await-Propel-Workshop``` 
<br>

 ### Step 2: Download project as .zip off of the branch selected.
<br>

### Step 3: Unzip the downloaded repo and open "Async Await Propel Workshop.xcodeproj"
<br>
 
### Step 4: Make sure you can compile the Xcode project.
<br>

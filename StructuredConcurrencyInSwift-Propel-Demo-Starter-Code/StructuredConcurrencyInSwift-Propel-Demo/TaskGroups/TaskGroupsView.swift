//
//  TaskGroupsView.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import SwiftUI

struct TaskGroupsView: View {
    
    @StateObject var taskGroupsViewModel: TaskGroupsViewModel = TaskGroupsViewModel()
    
    var body: some View {
        Button("Randomly Select 5 Words", action: {
            Task {
                await taskGroupsViewModel.getFiveNewWords()
            }
        })
        
        List(taskGroupsViewModel.generatedStrings, id: \.self) { word in
            Text(word)
        }
    }
}

//
//  TaskGroupsViewModel.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import Foundation

class TaskGroupsViewModel: ObservableObject {
    init() {}
    
    // this is a list of commonly used words in the English Language
    let words = ["all", "and", "boy", "book", "call", "car", "chair", "children", "city", "dog", "door", "human", "enemy", "end","enough",
    "eat", "father", "friend", "go", "good", "girl", "food", "hear", "house", "inside", "laugh", "listen", "man", "name", "never", "new",
    "next", "noise", "often", "pair", "pick", "play", "room", "sell", "sister", "sit", "smile", "speak", "then", "think", "walk", "water",
    "woman", "work", "write"]
    
    @Published var generatedStrings = [String]()
    
    func getFiveNewWords() async -> [String] {
        // Task Group Code Will Go Here
        
        // 1. Use 'withTaskGroup' to get 5 random strings
        // 2. use the results of the task group to group the results into an array of strings
        
        return [String]()
    }
    
    func randomString() async -> String {
        let choice: Int = Int.random(in: 1..<words.count)
        return words[choice]
    }
}

//
//  StructuredConcurrencyInSwift_Propel_DemoApp.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import SwiftUI

@main
struct StructuredConcurrencyInSwift_Propel_DemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  AsyncLetViewModel.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import SwiftUI

class AsyncLetViewModel: ObservableObject {
    
    @Published var character: Hero?
    
    init() {}
    
    func generateHero() {
        Task {
            // 1. Generate the character stats
            // 2. Use the generated character stats to initialize the character object
        }
    }
    
    func generateAge() async -> Int{
        return Int.random(in: 1..<100)
    }
    
    func generateHeight() async -> Int {
        return Int.random(in: 12..<80)
    }
    
    func generateEyeCount() async -> Int {
        return Int.random(in: 1..<5)
    }
    
    func generateHairType() async -> HairType {
        guard let type = HairType.allCases.randomElement() else {
            return HairType.bald
        }
        return type
    }
}

//
//  Hero.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import Foundation

enum HairType: CaseIterable {
    case bald
    case mullet
    case braids
    case slickedBack
}

struct Hero {
    var age: Int
    var height: Int
    var eyeCount: Int
    var hairType: HairType
    
    init(age: Int, height: Int, eyeCount: Int, hairType: HairType) {
        self.age = age
        self.height = height
        self.eyeCount = eyeCount
        self.hairType = hairType
    }
}

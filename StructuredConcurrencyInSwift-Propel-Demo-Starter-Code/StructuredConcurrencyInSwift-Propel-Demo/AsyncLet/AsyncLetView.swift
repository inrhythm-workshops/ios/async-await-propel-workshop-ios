//
//  AsyncLetView.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import SwiftUI

struct AsyncLetView: View {
    
    @StateObject var asyncLetViewModel: AsyncLetViewModel = AsyncLetViewModel()
    
    var body: some View {
        Spacer()
        Text("Character Stats Generator:")
            .font(.system(size: 20))
            .padding()
        
        Button("Generator Character Stats", action: {
            asyncLetViewModel.generateHero()
        })
        .padding(.bottom)
        VStack(alignment: .leading) {
            HStack {
                Text("Age: ")
                if asyncLetViewModel.character != nil {
                    Text(String(asyncLetViewModel.character?.age ?? 0))
                } else { EmptyView() }
            }
            HStack {
                Text("Height:")
                if asyncLetViewModel.character != nil {
                    Text(String(asyncLetViewModel.character?.height ?? 0))
                } else { EmptyView() }
            }
            HStack {
                Text("Eye Count:")
                if asyncLetViewModel.character != nil {
                    Text(String(asyncLetViewModel.character?.eyeCount ?? 0))
                } else { EmptyView() }
            }
            HStack {
                Text("Hair Style:")
                if asyncLetViewModel.character != nil {
                    switch asyncLetViewModel.character?.hairType {
                    case .braids:
                        Text("Gul D Locks")
                    case .none:
                        Text("What Neck?")
                    case .some(.bald):
                        Text("Cue Ball")
                    case .some(.mullet):
                        Text("Pa Artie")
                    case .some(.slickedBack):
                        Text("1980s Business Person")
                    }
                } else { EmptyView() }
            }
        }
        
        Spacer()
        
    }
}

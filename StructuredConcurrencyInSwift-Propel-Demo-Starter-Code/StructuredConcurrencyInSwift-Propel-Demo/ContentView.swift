//
//  ContentView.swift
//  StructuredConcurrencyInSwift-Propel-Demo
//
//  Created by Joshua Buchanan on 3/27/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            AsyncLetView()
            //TaskGroupsView()
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
